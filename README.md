prerequisites `abiword`, `python3`, `pip`

```bash
sudo apt-get install -y abiword
```

```bash
sudo apt-get install -y python3 pip
```
# Ishga qabul qilishda kerak bo'ladigan hujjat uchun request
```POST``` /generate-doc/ishga-qabul
```bash
{
    "data": {
        "yil": "2023",
        "kun":"12",
        "oy":"May",
        "soni": "4",
        "ism": "Х. Хикматов",
        "qomita": "OSG",
        "lavozim": "Go Developer",
        "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
        "imzalovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "ishga_qabul",
    "word_path": "./word",
    "pdf_path": "./pdf"
}
```
### Response pdf va word fayllarning saqlangan manzili request
```bash
{
  "message": None,
  "status": False,
  "data": {
    "pdf": "http://kadr.e-senat.uz/generated-doc/pdf/O'zgartirish-42344.pdf",
    "word": "http://kadr.e-senat.uz/generated-doc/word/O'zgartirish-42344.docx"
  }
}
```

# Xodimlar xizmat safariga ketganda kerak bo'ladigan hujjat uchun request
```POST``` /generate-doc/xizmat-safar
```bash
{
    "data": {
        "yil":"2023",
        "kun":"12",
        "oy":"May",
        "soni":"5",
        "ism":"Akbarshoh Ismoilov",
        "qomita_nomi":"Қўмита, бошқарма ёки бўлим номи агар бўлса таркибий қисми номи",
        "buxgalter_ismi":"Akbarshoh Ismoilov",
        "imzolovchi":"Shaxsiy Ism"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "Xizamat",
    "word_path": "./word",
    "pdf_path": "./pdf"
}
```
### Response pdf va word fayllarning saqlangan manzili request
```bash
{
  "message": None,
  "status": False,
  "data": {
    "pdf": "http://kadr.e-senat.uz/generated-doc/pdf/O'zgartirish-42344.pdf",
    "word": "http://kadr.e-senat.uz/generated-doc/word/O'zgartirish-42344.docx"
  }
}
```

# Xodim lovozimi o'zgarganda kerak bo'ladigan hujjat uchun request
```POST``` /generate-doc/ozgartirish
```bash
{
    "yil": 2023,
    "kun": 3,
    "oy": "may",
    "soni": 42344,
    "ism": "Х. Хикматов",
    "yangi_lavozim": "OSG middle developer",
    "eski_lavozim": "OSG junior developer",
    "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
    "mexnat_raqami":12341,
    "imzolovchi": "Х. Хикматов"
}
```
### Response pdf va word fayllarning saqlangan manzili
```bash
{
  "message": None,
  "status": False,
  "data": {
    "pdf": "http://kadr.e-senat.uz/generated-doc/pdf/O'zgartirish-42344.pdf",
    "word": "http://kadr.e-senat.uz/generated-doc/word/O'zgartirish-42344.docx"
  }
}
```

# Xodim ishdan olinganda to'ldirilishi kerak bo'ladigan hujjet uchun request
```POST``` /generate-doc/ishdan-olish
```bash
{
  {
    "data":{
        "yil": 2023,
        "kun": 3,
        "oy": "may",
        "soni": 42344,
        "ism": "Х. Хикматов",
        "yangi_lavozim": "OSG middle developer",
        "eski_lavozim": "OSG junior developer",
        "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
        "mexnat_raqami":12341,
        "imzolovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "ishga_qabul",
    "word_path": "./word",
    "pdf_path": "./pdf"
}
```
### Response pdf va word fayllarning saqlangan manzili
```bash
{
  "message": None,
  "status": False,
  "data": {
    "pdf": "http://kadr.e-senat.uz/generated-doc/pdf/O'zgartirish-42344.pdf",
    "word": "http://kadr.e-senat.uz/generated-doc/word/O'zgartirish-42344.docx"
  }
}
```

### Xizmat safari guvohnomasi hujjati uchun request
```POST``` /generate-doc/guvohnoma
```bash
{
    "data":{
        "yil": 2023,
        "kun": 3,
        "oy": "may",
        "soni": 42344,
        "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
        "lavozim": "OSG middle developer",
        "safar_maqsadi":"2023-yil birinchi yarim yilligiga mo‘ljallangan ish rejasiga muvofiq yoshlar sanoat va tadbirkorlik zonalari faoliyatini tashkil etish hamda yoshlarning tadbirkorlikka oid tashabbuslarini qo‘llab-quvvatlash bo‘yicha amalga oshirilayotgan ishlar holatini Yoshlar, madaniyat va sport masalalari qo‘mitasi tomonidan o‘rganishda ishtirok etish uchun",
        "muddadi": 8,
        "sanalar":"17-25-may",
        "farmoyish_soni":12341,
        "imzalovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "guvohnoma",
    "word_path": "./word",
    "pdf_path": "./pdf"
}
```
### Response pdf va word fayllarning saqlangan manzili
```bash
{
  "message": None,
  "status": False,
  "data": {
    "pdf": "http://kadr.e-senat.uz/generated-doc/pdf/O'zgartirish-42344.pdf",
    "word": "http://kadr.e-senat.uz/generated-doc/word/O'zgartirish-42344.docx"
  }
}
```