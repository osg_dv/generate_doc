# Python 3 asosiy imoji asosida konteyner o'rnatish
FROM python:3

# Loyiha katalogini konteynerda joylash
WORKDIR /app

# Host katalogidagi barcha fayllarni loyiha katalogiga nusxalash
COPY . /app

# Loyiha uchun talabnomalarni o'rnatish
RUN pip install -r requirements.txt

# Konteynerda loyiha tomonidan ishga tushiriladigan buyruq
CMD [ "python", "app.py" ]