import argparse
import json
from http.server import BaseHTTPRequestHandler, HTTPServer

from word import Xizmat_safari, Ishga_qabul, Ozgartirish, Ishdan_olish, Guvohnoma

class MyServer(BaseHTTPRequestHandler):
    def do_POST(self):
        if self.path == "/generate-doc/ishga-qabul":
            content_len = int(self.headers.get('Content-Length'))
            post_body = self.rfile.read(content_len)
            json_decoded = post_body.decode("utf-8").replace("'", '"')
            data = json.loads(json_decoded)
            res, code=Ishga_qabul(data)
            self._set_response(code)
            json_object = json.dumps(res, indent=4, sort_keys=True)
            self.wfile.write(json_object.encode("utf-8"))
        elif self.path == "/generate-doc/xizmat-safar":
            content_len = int(self.headers.get('Content-Length'))
            post_body = self.rfile.read(content_len)
            json_decoded = post_body.decode("utf-8").replace("'", '"')
            data = json.loads(json_decoded)
            res, code=Xizmat_safari(data)
            self._set_response(code)
            json_object = json.dumps(res, indent=4, sort_keys=True)
            self.wfile.write(json_object.encode("utf-8"))
        elif self.path == "/generate-doc/ozgartirish":
            content_len = int(self.headers.get('Content-Length'))
            post_body = self.rfile.read(content_len)
            json_decoded = post_body.decode("utf-8").replace("'", '"')
            data = json.loads(json_decoded)
            res,code =Ozgartirish(data)
            self._set_response(code)
            json_object = json.dumps(res, indent=4, sort_keys=True)
            self.wfile.write(json_object.encode("utf-8"))
        elif self.path == "/generate-doc/ishdan-olish":
            content_len = int(self.headers.get('Content-Length'))
            post_body = self.rfile.read(content_len)
            json_decoded = post_body.decode("utf-8").replace("'", '"')
            data = json.loads(json_decoded)
            res,code=Ishdan_olish(data)
            self._set_response(code)
            json_object = json.dumps(res, indent=4, sort_keys=True)
            self.wfile.write(json_object.encode("utf-8"))
        elif self.path == "/generate-doc/guvohnoma":
            content_len = int(self.headers.get('Content-Length'))
            post_body = self.rfile.read(content_len)
            json_decoded = post_body.decode("utf-8").replace("'", '"')
            data = json.loads(json_decoded)
            res,code=Guvohnoma(data)
            self._set_response(code)
            json_object = json.dumps(res, indent=4, sort_keys=True)
            self.wfile.write(json_object.encode("utf-8"))


    def _set_response(self, code):
        self.send_response(code)
        self.send_header('Content-type', 'application/json')
        self.end_headers()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', default='localhost', type=str)
    parser.add_argument('--port', default=8000, type=int)

    args = parser.parse_args()

    webServer = HTTPServer((args.ip, args.port), MyServer)
    print("Server started http://%s:%s" % (args.ip, args.port))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")