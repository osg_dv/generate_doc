from docx import Document

import subprocess

import os
from docx.shared import Pt, Inches, Cm

from docx.enum.text import WD_PARAGRAPH_ALIGNMENT

b = {
    "data": {
        "yil": "2023",
        "kun":"12",
        "oy":"May",
        "soni": "4",
        "ism": "Х. Хикматов",
        "qomita": "OSG",
        "lavozim": "Go Developer",
        "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
        "imzalovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "ishga_qabul",
    "word_path": "./word",
    "pdf_path": "./pdf"
}

def Ishga_qabul(a):
    try:
        base_url=a['base_url']
        file_name=a['file_name']
        pdf_path=a['pdf_path']
        word_path=a['word_path']
    except Exception as ex:
        return {
            "message": f"base_data is not valid, error:{ex}",
            "status": False,
            "data": None
        }, 400
    try:
        if not os.path.exists(pdf_path):
            os.mkdir(pdf_path)
        if not os.path.exists(word_path):
            os.mkdir(word_path)
    except Exception as ex:
        return {
                   "message": f"path has an error: {ex}",
                   "status": False,
                   "data": None
        }, 400
    word_name = os.path.abspath(f"{word_path}/{file_name}.docx")
    pdf_name = os.path.abspath(f"{pdf_path}/{file_name}.pdf")

    if os.path.exists(word_name):
        os.remove(word_name)
    if os.path.exists(pdf_name):
        os.remove(pdf_name)
    try:
        document = Document()
        try:
            p1 = document.add_paragraph()
            p1.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            p1.paragraph_format.left_indent = Pt(-20)
            p1.paragraph_format.right_indent = Pt(-42)
            run1 = p1.add_run(f"\n\n{a['data']['yil']}-yil  {a['data']['kun']}-{a['data']['oy']}                                            {a['data']['soni']}-sonli		                             Toshkent sh.")
            run1.font.size = Pt(12)
            run1.font.name="Times New Roman"
            run2 = p1.add_run(f"\n\n\n\n{a['data']['ism']}ni O‘zbekiston Respublikasi Oliy Majlisi Senati devoni\n{a['data']['qomita']}  {a['data']['lavozim']}si\nlavozimiga ishga qabul qilish to‘g‘risida\n\n")
            run2.font.size = Pt(14)
            run2.font.bold = True
            run2.font.name = "Times New Roman"

            p3 = document.add_paragraph()
            p3.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p3.paragraph_format.left_indent = Pt(-20)
            p3.paragraph_format.right_indent = Pt(-42)

            run3 = p3.add_run(
                f"\t1. {a['data']['toliq_ism']}– 2023-yilning 1-mayidan e’tiboran uch oylik sinov muddati bilan O‘zbekiston Respublikasi Oliy Majlisi Senati devoni {a['data']['qomita']}   {a['data']['lavozim']} lavozimiga ishga qabul qilinsin.")
            run3.font.size = Pt(14)
            run3.font.name = "Times New Roman"
            
            p6 = document.add_paragraph()
            p6.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p6.paragraph_format.left_indent = Pt(-20)
            p6.paragraph_format.right_indent = Pt(-42)
            
            run6 = p6.add_run(
                f"\t2. Oylik maoshi shtat jadvaliga muvofiq belgilansin.")
            run6.font.size = Pt(14)
            run6.font.name = "Times New Roman"
            
            p4 = document.add_paragraph()
            p4.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p4.paragraph_format.left_indent = Pt(-20)
            p4.paragraph_format.right_indent = Pt(-42)

            run4 = p4.add_run(
                f"\n\n\tAsos: {a['data']['ism']} bilan {a['data']['yil']}-yil {a['data']['sana']}da tuzilgan {a['data']['soni']}-sonli mehnat shartnomasi va uning arizasi.\n\n\n")
            run4.font.size = Pt(14)
            run4.font.name = "Times New Roman"

            p3 = document.add_paragraph()
            p3.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
            p3.paragraph_format.left_indent = Pt(-20)
            p3.paragraph_format.right_indent = Pt(-42)

            run5 = p3.add_run(
                f"{a['data']['imzalovchi']}")
            run5.font.size = Pt(14)
            run5.font.name = "Times New Roman"
        except Exception as ex:
            return {
                "message":"one of body fields is not correct",
                "status": False,
                "data":None
            }, 400

        document.save(word_name)
        command = ["abiword", "--to=pdf", word_name]
        subprocess.run(command)
        mv = ['mv', f"{word_path}/{file_name}.pdf", pdf_path]
        subprocess.run(mv)
        return {
            "message": "document successfully created",
            "status": True,
            "data": {
                "pdf_file":f"{base_url}{file_name}.pdf",
                "word_file":f"{base_url}{file_name}.docx"
            }
        }, 200
    except Exception as ex:
        return {
            "message": f"has error: {ex}",
            "status": False,
            "data":None
        }, 500

xizmat = {
    "data": {
        "yil":"2023",
        "kun":"12",
        "oy":"May",
        "soni":"5",
        "ism":"Akbarshoh Ismoilov",
        "qomita_nomi":"Қўмита, бошқарма ёки бўлим номи агар бўлса таркибий қисми номи",
        "buxgalter_ismi":"Akbarshoh Ismoilov",
        "imzolovchi":"Shaxsiy Ism"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "ishga_qabul",
    "word_path": "./word",
    "pdf_path": "./pdf"
}

def Xizmat_safari(body):
    try:
        base_url = body['base_url']
        file_name = body['file_name']
        pdf_path = body['pdf_path']
        word_path = body['word_path']
    except Exception as ex:
        return {
            "message":f"base_data is not valid, error:{ex}",
            "status": False,
            "data":None
        }, 400
    try:
        if not os.path.exists(pdf_path):
            os.mkdir(pdf_path)
        if not os.path.exists(word_path):
            os.mkdir(word_path)
    except Exception as ex:
        return {
                   "message": f"path has an error: {ex}",
                   "status": False,
                   "data": None
               }, 400
    word_name = os.path.abspath(f"{word_path}/{file_name}.docx")
    pdf_name = os.path.abspath(f"{pdf_path}/{file_name}.pdf")

    if os.path.exists(word_name):
        os.remove(word_name)
    if os.path.exists(pdf_name):
        os.remove(pdf_name)
    try:
        document = Document()
        try:
            p1=document.add_paragraph()
            p1.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            p1.paragraph_format.left_indent = Pt(-20)
            p1.paragraph_format.right_indent = Pt(-42)
            run1=p1.add_run(f"\n\n{body['data']['yil']}-yil  {body['data']['kun']}-{body['data']['oy']}                                          {body['data']['soni']}-sonli		                            Toshkent sh.")
            run1.font.size = Pt(12)
            run1.font.name='Times New Roman'
            
            run2=p1.add_run(f"\n\n\n{body['data']['ism']}ni xizmat safariga yuborish to'g'risida")
            run2.font.size = Pt(14)
            run2.font.bold = True
            run2.font.name = "Times New Roman"
            
            p3 = document.add_paragraph()
            p3.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p3.paragraph_format.left_indent = Pt(-20)
            p3.paragraph_format.right_indent = Pt(-42)
            run3 = p3.add_run(
                f"\n\n\n\t1. O‘zbekiston Respublikasi Oliy Majlisi Senati {body['data']['qomita_nomi']}ning 2023-yil birinchi yarim yilligiga mo‘ljallangan ish rejasiga muvofiq yoshlar sanoat va tadbirkorlik zonalari faoliyatini tashkil etish hamda yoshlarning tadbirkorlikka oid tashabbuslarini qo‘llab-quvvatlash bo‘yicha amalga oshirilayotgan ishlar holatini Yoshlar, madaniyat va sport masalalari qo‘mitasi tomonidan o‘rganishda ishtirok etish uchun mazkur qo‘mita kotibiyati bosh maslahatchisi A. Turdiyev hamda Davlat budjeti va hududlarni ijtimoiy-iqtisodiy rivojlantirish masalalarini tahlil qilish boshqarmasi bosh maslahatchisi E. Abdukabirov to‘rt kunga (2023-yil 22 – 25-may) Andijon va Farg‘ona viloyatlariga xizmat safariga yuborilsin.")
            run3.font.size = Pt(14)
            run3.font.name = "Times New Roman"
            
            p4 = document.add_paragraph()
            p4.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p4.paragraph_format.left_indent = Pt(-20)
            p4.paragraph_format.right_indent = Pt(-42)
            run4 = p4.add_run(
                f"\t2. Senat devoni Buxgalteriya xizmati rahbari – Bosh buxgalter ({body['data']['buxgalter_ismi']}) ushbu xizmat safari bilan bog‘liq xarajatlarni haqiqiy xarajatlardan kelib chiqib, belgilangan tartibda moliyalashtirishni amalga oshirsin.")
            run4.font.size = Pt(14)
            run4.font.name = "Times New Roman"
            
            p5 = document.add_paragraph()
            p5.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p5.paragraph_format.left_indent = Pt(-20)
            p5.paragraph_format.right_indent = Pt(-42)
            run5 = p5.add_run(
                f"\n\n\n\tAsos: {body['data']['qomita_nomi']} {body['data']['ism']}ning bildirgisi.")
            run5.font.size = Pt(14)
            run5.font.name = "Times New Roman"
            
            p6 = document.add_paragraph()
            p6.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
            p6.paragraph_format.left_indent = Pt(-20)
            p6.paragraph_format.right_indent = Pt(-42)
            run6 = p6.add_run(
                f"\n\n\n{body['data']['imzolovchi']}")
            run6.font.size = Pt(14)
            run6.font.name = "Times New Roman"
        except Exception as ex:
            return {
                "message":"one of body fields is not correct",
                "status": False,
                "data":None
            }, 400
        document.save(word_name)
        command = ["abiword", "--to=pdf", word_name]
        subprocess.run(command)
        mv = ['mv', f"{word_path}/{file_name}.pdf", pdf_path]
        subprocess.run(mv)
        return {
            "message": "document successfully created",
            "status": True,
            "data": {
                "pdf_file":f"{base_url}{file_name}.pdf",
                "word_file":f"{base_url}{file_name}.docx"
            }
        }, 200
    except Exception as ex:
        return {
            "message": f"has error: {ex}",
            "status": False,
            "data":None
        }, 500

olish={
"data": {
            "yil": 2023,
            "kun": 3,
            "oy": "may",
            "soni": 42344,
            "ism": "Х. Хикматов",
            "qomita": "OSG",
            "buxgalter_ism":"A. Imoyilov",
            "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
            "imzalovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "ishga_olish",
    "word_path": "./word",
    "pdf_path": "./pdf"
}

def Ishdan_olish(a):
    try:
        base_url = a['base_url']
        file_name = a['file_name']
        pdf_path = a['pdf_path']
        word_path = a['word_path']
    except Exception as ex:
        return {
                   "message": f"base_data is not valid, error:{ex}",
                   "status": False,
                   "data": None
               }, 400
    try:
        if not os.path.exists(pdf_path):
            os.mkdir(pdf_path)
        if not os.path.exists(word_path):
            os.mkdir(word_path)
    except Exception as ex:
        return {
                   "message": f"path has an error: {ex}",
                   "status": False,
                   "data": None
               }, 400
    word_name = os.path.abspath(f"{word_path}/{file_name}.docx")
    pdf_name = os.path.abspath(f"{pdf_path}/{file_name}.pdf")

    if os.path.exists(word_name):
        os.remove(word_name)
    if os.path.exists(pdf_name):
        os.remove(pdf_name)
    try:
        document = Document()
        try:
            p1=document.add_paragraph()
            p1.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            p1.paragraph_format.left_indent = Pt(-20)
            p1.paragraph_format.right_indent = Pt(-42)
            run1=p1.add_run(f"\n\n{a['data']['yil']}-yil  {a['data']['kun']}-{a['data']['oy']}                                            {a['data']['soni']}-sonli                                 Toshkent sh.")
            run1.font.size = Pt(12)
            run1.font.name="Times New Roman"

            run2=p1.add_run(f"\n\n\n\n{a['data']['ism']} bilan tuzilgan mehnat shartnomasini bekor qilish to‘g‘risida\n\n")
            run2.font.size = Pt(14)
            run2.font.bold = True
            run2.font.name = "Times New Roman"

            p2 = document.add_paragraph()
            p2.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p2.paragraph_format.left_indent = Pt(-20)
            p2.paragraph_format.right_indent = Pt(-42)

            run3 = p2.add_run(
                f"\tO‘zbekiston Respublikasi Oliy Majlisi Senati {a['data']['qomita']} {a['data']['toliq_ism']} o‘z arizasiga ko‘ra {a['data']['yil']}-yil {a['data']['kun']}-{a['data']['oy']} idan e’tiboran lavozimidan ozod etilsin. U bilan tuzilgan mehnat shartnomasi bekor qilinsin.")
            run3.font.size = Pt(14)
            run3.font.name = "Times New Roman"

            p4 = document.add_paragraph()
            p4.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p4.paragraph_format.left_indent = Pt(-20)
            p4.paragraph_format.right_indent = Pt(-42)

            run4 = p4.add_run(f"\tSenat devoni Buxgalteriya xizmati rahbari – Bosh buxgalter ({a['data']['buxgalter_ism']}) {a['data']['ism']} bilan belgilangan tartibda hisob-kitob qilsin.")
            run4.font.size = Pt(14)
            run4.font.name = "Times New Roman"

            p5 = document.add_paragraph()
            p5.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p5.paragraph_format.left_indent = Pt(-20)
            p5.paragraph_format.right_indent = Pt(-42)

            run6 = p5.add_run(
                f"\n\n\tAsos: {a['data']['ism']}ning arizasi.")
            run6.font.size = Pt(14)
            run6.font.name = "Times New Roman"

            p3 = document.add_paragraph()
            p3.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
            p3.paragraph_format.left_indent = Pt(-20)
            p3.paragraph_format.right_indent = Pt(-42)

            run5 = p3.add_run(
                f"\n\n{a['data']['imzalovchi']}")
            run5.font.size = Pt(14)
            run5.font.name = "Times New Roman"
        except Exception as ex:
            return {
                       "message": "one of body fields is not correct",
                       "status": False,
                       "data": None
                   }, 400
        document.save(word_name)
        command = ["abiword", "--to=pdf", word_name]
        subprocess.run(command)
        mv = ['mv', f"{word_path}/{file_name}.pdf", pdf_path]
        subprocess.run(mv)
        return {
                   "message": "document successfully created",
                   "status": True,
                   "data": {
                       "pdf_file": f"{base_url}{file_name}.pdf",
                       "word_file": f"{base_url}{file_name}.docx"
                   }
               }, 200
    except Exception as ex:
        return {
            "message": f"has error: {ex}",
            "status": False,
            "data": None
        }, 500


ozgartirish = {
    "data":{
        "yil": 2023,
        "kun": 3,
        "oy": "may",
        "soni": 42344,
        "ism": "Х. Хикматов",
        "yangi_lavozim": "OSG middle developer",
        "eski_lavozim": "OSG junior developer",
        "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
        "mexnat_raqami":12341,
        "imzolovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "ishga_qabul",
    "word_path": "./word",
    "pdf_path": "./pdf"
}

def Ozgartirish(a):
    try:
        base_url = a['base_url']
        file_name = a['file_name']
        pdf_path = a['pdf_path']
        word_path = a['word_path']
    except Exception as ex:
        return {
                   "message": f"base_data is not valid, error:{ex}",
                   "status": False,
                   "data": None
               }, 400
    try:
        if not os.path.exists(pdf_path):
            os.mkdir(pdf_path)
        if not os.path.exists(word_path):
            os.mkdir(word_path)
    except Exception as ex:
        return {
                   "message": f"path has an error: {ex}",
                   "status": False,
                   "data": None
               }, 400
    word_name = os.path.abspath(f"{word_path}/{file_name}.docx")
    pdf_name = os.path.abspath(f"{pdf_path}/{file_name}.pdf")

    if os.path.exists(word_name):
        os.remove(word_name)
    if os.path.exists(pdf_name):
        os.remove(pdf_name)
    try:
        document = Document()
        try:

            p1=document.add_paragraph()
            p1.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            p1.paragraph_format.left_indent = Pt(-20)
            p1.paragraph_format.right_indent = Pt(-42)
            run1=p1.add_run(f"\n\n{a['data']['yil']}-yil  {a['data']['kun']}-{a['data']['oy']}                                            {a['data']['soni']}-sonli                                 Toshkent sh.")
            run1.font.size = Pt(12)
            run1.font.name="Times New Roman"

            run2=p1.add_run(f"\n\n\n\n{a['data']['ism']}ni O‘zbekiston Respublikasi Oliy Majlisi {a['data']['yangi_lavozim']} lavozimiga o‘tkazish to‘g‘risida\n\n")
            run2.font.size = Pt(14)
            run2.font.bold = True
            run2.font.name = "Times New Roman"

            p2 = document.add_paragraph()
            p2.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p2.paragraph_format.left_indent = Pt(-20)
            p2.paragraph_format.right_indent = Pt(-42)

            run3 = p2.add_run(
                f"\tO‘zbekiston Respublikasi Oliy Majlisi {a['data']['eski_lavozim']} {a['data']['toliq_ism']} {a['data']['yil']}-yil {a['data']['kun']}-{a['data']['oy']}dan e’tiboran O‘zbekiston Respublikasi Oliy Majlisi {a['data']['yangi_lavozim']} lavozimiga o‘tkazilsin.")
            run3.font.size = Pt(14)
            run3.font.name = "Times New Roman"

            p4 = document.add_paragraph()
            p4.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p4.paragraph_format.left_indent = Pt(-20)
            p4.paragraph_format.right_indent = Pt(-42)

            run4 = p4.add_run(f"\tOylik maosh shtat jadvaliga muvofiq belgilansin.")
            run4.font.size = Pt(14)
            run4.font.name = "Times New Roman"

            p5 = document.add_paragraph()
            p5.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
            p5.paragraph_format.left_indent = Pt(-20)
            p5.paragraph_format.right_indent = Pt(-42)

            run6 = p5.add_run(
                f"\n\n\tAsos: {a['data']['mexnat_raqami']}-sonli mehnat shartnomasiga kiritilgan o‘zgartirish, {a['data']['ism']}ning roziligi.")
            run6.font.size = Pt(14)
            run6.font.name = "Times New Roman"

            p3 = document.add_paragraph()
            p3.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
            p3.paragraph_format.left_indent = Pt(-20)
            p3.paragraph_format.right_indent = Pt(-42)

            run5 = p3.add_run(
                f"\n\n{a['data']['imzolovchi']}")
            run5.font.size = Pt(14)
            run5.font.name = "Times New Roman"
        except Exception as ex:
            return {
                       "message": "one of body fields is not correct",
                       "status": False,
                       "data": None
                   }, 400
        document.save(word_name)
        command = ["abiword", "--to=pdf", word_name]
        subprocess.run(command)
        mv = ['mv', f"{word_path}/{file_name}.pdf", pdf_path]
        subprocess.run(mv)
        return {
                   "message": "document successfully created",
                   "status": True,
                   "data": {
                       "pdf_file": f"{base_url}{file_name}.pdf",
                       "word_file": f"{base_url}{file_name}.docx"
                   }
               }, 200
    except Exception as ex:
        return {
            "message": f"has error: {ex}",
            "status": False,
            "data": None
        }, 500


guvohnoma={
    "data":{
        "yil": 2023,
        "kun": 3,
        "oy": "may",
        "soni": 42344,
        "toliq_ism": "Xurshidjon Shukurjon o‘g‘li Egamov",
        "lavozim": "OSG middle developer",
        "safar_maqsadi":"2023-yil birinchi yarim yilligiga mo‘ljallangan ish rejasiga muvofiq yoshlar sanoat va tadbirkorlik zonalari faoliyatini tashkil etish hamda yoshlarning tadbirkorlikka oid tashabbuslarini qo‘llab-quvvatlash bo‘yicha amalga oshirilayotgan ishlar holatini Yoshlar, madaniyat va sport masalalari qo‘mitasi tomonidan o‘rganishda ishtirok etish uchun",
        "muddadi": 8,
        "sanalar":"17-25-may",
        "farmoyish_soni":12341,
        "imzalovchi": "Х. Хикматов"
    },
    "base_url": "http://kadr.e-senat.uz/generated-doc/doc/",
    "file_name": "guvohnoma",
    "word_path": "./word",
    "pdf_path": "./pdf"
}

def Guvohnoma(a):
    try:
        base_url = a['base_url']
        file_name = a['file_name']
        pdf_path = a['pdf_path']
        word_path = a['word_path']
    except Exception as ex:
        return {
                   "message": f"base_data is not valid, error:{ex}",
                   "status": False,
                   "data": None
               }, 400
    try:
        if not os.path.exists(pdf_path):
            os.mkdir(pdf_path)
        if not os.path.exists(word_path):
            os.mkdir(word_path)
    except Exception as ex:
        return {
                   "message": f"path has an error: {ex}",
                   "status": False,
                   "data": None
               }, 400
    word_name = os.path.abspath(f"{word_path}/{file_name}.docx")
    pdf_name = os.path.abspath(f"{pdf_path}/{file_name}.pdf")

    if os.path.exists(word_name):
        os.remove(word_name)
    if os.path.exists(pdf_name):
        os.remove(pdf_name)
    try:
        document = Document()
        try:
            document.sections[0].left_margin = Cm(1.5)
            document.sections[0].top_margin = Cm(1)

            table = document.add_table(rows=1, cols=2)
            table.allow_autofit = False

            table.columns[0].width = Cm(6)
            table.columns[1].width = Cm(13)

            cell_1 = table.cell(0, 0)
            p1=cell_1.add_paragraph()
            p1.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

            image_path= "images/gerb.png"
            run1 = p1.add_run()
            run1.add_picture(image_path, width=Inches(1.5), height=Inches(1.5))

            run2 = p1.add_run(f"\nO‘zbekiston Respublikasi Oliy Majlisining Senati")
            run2.font.size = Pt(13)
            run2.font.bold = True
            run2.font.name = "Arial"

            run3 = p1.add_run(f"\n\n\n\n{a['data']['yil']}-yil   {a['data']['kun']}-{a['data']['oy']}\n{a['data']['soni']}-sonli\n\n\nToshkent")
            run3.font.size = Pt(11)
            run3.font.name = "Arial"

            cell_2 = table.cell(0, 1)
            p2 = cell_2.add_paragraph()
            p2.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

            run1 = p2.add_run("XIZMAT  SAFARI  GUVOHNOMASI\n\n")
            run1.font.size = Pt(14)
            run1.font.bold = True
            run1.font.name = "Arial"

            run2 = p2.add_run(f"{a['data']['toliq_ism']}\n\n{a['data']['lavozim']}\n")
            run2.font.size = Pt(14)
            run2.font.name = "Arial"

            p3 = cell_2.add_paragraph()
            p3.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY

            run1 = p3.add_run(f"Safar {a['data']['safar_maqsadi']}")
            run1.font.size = Pt(12)
            run1.font.name = "Times New Roman"

            p4 = cell_2.add_paragraph()
            p4.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT

            run1 = p4.add_run(f"{a['data']['muddadi']} kunga {a['data']['yil']}-yil    {a['data']['sanalar']}\n")
            run1.font.size = Pt(12)
            run1.font.name = "Times New Roman"

            run2 = p4.add_run(
                f"Asos: {a['data']['yil']} -yil   {a['data']['kun']}-{a['data']['oy']}       {a['data']['farmoyish_soni']}-sonli Farmoyish\n\n")
            run2.font.size = Pt(12)
            run2.font.name = "Times New Roman"

            run3 = p4.add_run(
                f"    O‘zbekiston Respublikasi\nOliy Majlisi Senati devoni rahbari    ")
            run3.font.size = Pt(12)
            run3.font.name = "Times New Roman"

            run4 = p4.add_run(f"                     ")
            run4.font.size = Pt(12)
            run4.underline = True
            run4.font.name = "Times New Roman"

            run5 = p4.add_run(f"  {a['data']['imzalovchi']}")
            run5.font.size = Pt(12)
            run5.font.name = "Times New Roman"


            p5 = document.add_paragraph()
            p5.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            run1 = p5.add_run(
                f"\n\n\n\nBo‘lgan joylari va jo‘nab ketgan vaqtlari haqidagi belgilar:")
            run1.font.size = Pt(14)
            run1.bold = True
            run1.font.name = "Times New Roman"

            table = document.add_table(rows=1, cols=2)
            table.allow_autofit = False

            table.columns[0].width = Cm(9.5)
            table.columns[1].width = Cm(9.5)

            cell_1 = table.cell(0, 0)
            p6=cell_1.add_paragraph()
            p6.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT

            for i in range(4):
                run1 = p6.add_run('Ketdi _________________________________________\n202___ - yil "______" ____________________________\n                                                    imzo\n')
                run1.font.size = Pt(10)
                run1.font.name = "Times New Roman"

            cell_2 = table.cell(0, 1)
            p7=cell_2.add_paragraph()
            p7.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT

            for i in range(4):
                run1 = p7.add_run('Keldi _________________________________________\n202___ - yil "______" ____________________________\n                                                    imzo\n')
                run1.font.size = Pt(10)
                run1.font.name = "Times New Roman"
        except Exception as ex:
            return {
                       "message": "one of body fields is not correct ",
                       "status": False,
                       "data": None
                   }, 400
        document.save(word_name)
        command = ["abiword", "--to=pdf", word_name]
        subprocess.run(command)
        mv = ['mv', f"{word_path}/{file_name}.pdf", pdf_path]
        subprocess.run(mv)
        return {
                   "message": "document successfully created",
                   "status": True,
                   "data": {
                       "pdf_file": f"{base_url}{file_name}.pdf",
                       "word_file": f"{base_url}{file_name}.docx"
                   }
               }, 200
    except Exception as ex:
        return {
            "message": f"has error: {ex}",
            "status": False,
            "data": None
        }, 500